import java.util.Scanner;
public class DoraProgram {
    public static void main(String[] args) {
        int rows,columns,number;
        int flag = 0;
        Scanner scanner = new Scanner(System.in);
        rows = scanner.nextInt();
        columns = scanner.nextInt();
        number = scanner.nextInt();
        for(int i =1; i < rows; i++) {
            if(number == i * columns + 1){
                flag = 1;
                break;
            }
        }
        if(number <= columns || flag == 1)
            System.out.println("yes");
        else
            System.out.println("No");
    }
}
